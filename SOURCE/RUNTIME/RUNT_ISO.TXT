Codepage 858 (ega.cpx) translations for RUNTIME
(Russian uses CP866, Polish uses CP790)

Portuguese 1 by Geraldo
segundos
passados
RUNTIME � um software livre de dom�nio p�blico por Eric Auer 2007
Uso: RUNTIME seu_programa [suas_op�?es]
Mostra quanto tempo o seu programa executou, max 1 dia (23
COMSPEC n?o encontrado

Spanish by Aitor
segundos
transcurridos
RUNTIME es software libre y dominio p�blico, por Eric Auer 2007
Uso: RUNTIME  programa [opciones]
Muestra el tiempo de ejecuci�n del programa, m�ximo 1 d�a (23
No se ha encontrado COMSPEC

French 1 by Stegozor
secondes
�coul�es
RUNTIME est un logiciel libre �crit et plac� dans le domaine public
par Eric Auer en 2007.
Utilisation : RUNTIME votre_logiciel [vos_options]
Montre combien de temps votre_logiciel a �t� ex�cut�, au maximum 1 jour (23
COMSPEC n'a pas pu �tre trouv�.

French 2 by Mateusz
secondes
�coul�es
RUNTIME est un logiciel libre du domaine public par Eric Auer 2007
Usage: RUNTIME ton_programme [tes_options]
Affiche le temps d'ex�cution de ton_programme, max 1 jour (23
COMSPEC n'a pas �t� trouv�

Turkish by Stegozor
saniye
ge�ti
RUNTIME Eric Auer tarafIndan 2007 senesinde programlanmI? kamu malI,
a�Ik kaynaklI bir yazIlImdIr.
KullanImI : RUNTIME programInIz [se�enekleriniz]
programInIzIn ne kadar zamandIr i?letildi?ini g�sterir,
maksimum 1 g�n (23
COMSPEC bulunamadI.

Portuguese 2 by Alain
segundos
decorridos
RUNTIME � um software livre, de dom�nio p�blico por Eric Auer 2007
Uso: RUNTIME seu_programa [op��es]
Mostra quanto tempo durou a execu��o do seu programa, max 1 dia (23
COMSPEC n�o encontrado

Italian by Roberto Mariottini
secondi
trascorsi
RUNTIME � un programma di pubblico dominio di Eric Auer 2007
Uso: RUNTIME programma [opzioni]
Mostra il tempo di esecuzione del programma, max 1 giorno (23
COMSPEC non trovato


